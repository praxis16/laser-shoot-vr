﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TimerScript : MonoBehaviour {
	float timeRemaining, timeLimit;
	TextMesh TimeText;
	public static bool gameOn;
	// Use this for initialization
	void Start () {
		gameOn = true;
		TimeText = GetComponent<TextMesh> ();
		timeLimit = 120f;
		timeRemaining = timeLimit - Time.timeSinceLevelLoad;
		DisplayTime ();
	}

	void DisplayTime() {
		int time = (int)timeRemaining;
		int min = time / 60;
		int sec = time % 60;
		string timeText;
		if (min == 0 && sec == 0) {
			timeText = "Game Over";
			gameOn = false;
		} else {
			timeText = string.Format ("{0:00}:{1:00}", min, sec);
		}
		TimeText.text = timeText;
	}
	
	// Update is called once per frame
	void Update () {
		if (timeRemaining >= 0) {
			timeRemaining = timeLimit - Time.timeSinceLevelLoad;
			DisplayTime ();
		} else if(Input.GetButtonDown("Fire1")) {
			int score = LaserScript.score;
			PlayerPrefs.SetInt ("lastScore", score);
			if (score > PlayerPrefs.GetInt ("highScore", 0)) {
				PlayerPrefs.SetInt ("highScore", score);
			}
			PlayerPrefs.Save ();
			SceneManager.LoadScene (0);
		}
	}
}
