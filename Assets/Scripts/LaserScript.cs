﻿using UnityEngine;
using System.Collections;

public class LaserScript : MonoBehaviour {
	LineRenderer line;
	public static int score;
	// Use this for initialization
	void Start () {
		
		line = gameObject.GetComponent<LineRenderer> ();
		line.enabled = false;
		score = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			StopCoroutine ("shootLaser");
			StartCoroutine ("shootLaser");
		}
	}

	public Vector3 SpawnRandomly() {
		Vector3 direction = Random.insideUnitSphere * 20f;
		direction.y = Mathf.Clamp(direction.y, 4f, 20f);

		return direction;
	}

	IEnumerator shootLaser() {
		line.enabled = true;
		AudioSource audio = transform.parent.GetComponent<AudioSource> ();
		if (!audio.isPlaying) {
			audio.Play ();
		}
		//bool respawned = false;
		while (Input.GetButton ("Fire1")) {
			line.GetComponent<Renderer>().material.mainTextureOffset = new Vector2 (0, Time.time);
			Ray ray = new Ray (transform.position, transform.forward);
			RaycastHit hit;
			line.SetPosition (0, ray.origin);
			if (Physics.Raycast (ray, out hit, 100)) {
				if (hit.rigidbody && TimerScript.gameOn) {
					if (hit.rigidbody.isKinematic) {
						AudioSource burst = hit.transform.gameObject.GetComponent<AudioSource> ();
						if (!burst.isPlaying) {
							burst.Play ();
						}
						GameObject ball = (GameObject)Instantiate (hit.transform.gameObject, SpawnRandomly (), Quaternion.identity);
						Rigidbody rigidBody = ball.GetComponent<Rigidbody> ();
						rigidBody.isKinematic = true;
						rigidBody.useGravity = false;
						score++;
					}
					hit.rigidbody.isKinematic = false;
					hit.rigidbody.useGravity = true;
					hit.rigidbody.AddForceAtPosition (transform.forward * 10, hit.point);
				}
				line.SetPosition (1, hit.point);
			} else {
				line.SetPosition (1, ray.GetPoint (100));
			}
			yield return null;
		}
		if (audio.isPlaying) {
			audio.Stop ();
		}
		line.enabled = false;
	}
}
