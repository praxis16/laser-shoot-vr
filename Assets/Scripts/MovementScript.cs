﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MovementScript : MonoBehaviour {
	//float speed;
	// Use this for initialization
	void Start () {
		//speed = 10f;
	}
	
	// Update is called once per frame
	void Update () {
		/*Vector3 dir = Vector3.zero;

		//map XY of device onto XZ of game
		dir.x = Input.acceleration.x;
		dir.z = -Input.acceleration.z;

		//clamp acceleration vector to unit sphere
		if (dir.sqrMagnitude > 1) {
			dir.Normalize ();
		}

		//move 10m in one second instead of one frame
		dir *= Time.deltaTime;
		//finally translate
		transform.Translate(dir * speed);*/
	}

	//called after normal update processes are done
	void LateUpdate() {
		GvrViewer.Instance.UpdateState();
		if (GvrViewer.Instance.BackButtonPressed) {
			int score = LaserScript.score;
			PlayerPrefs.SetInt ("lastScore", score);
			if (score > PlayerPrefs.GetInt ("highScore", 0)) {
				PlayerPrefs.SetInt ("highScore", score);
			}
			PlayerPrefs.Save ();
			SceneManager.LoadScene (0);
		}
	}
}
