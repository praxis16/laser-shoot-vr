﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class DestroyLandedSpikeBalls : MonoBehaviour {
	Regex regex;
	// Use this for initialization
	void Start () {
		regex = new Regex ("SpikeBall");
	}

	void OnCollisionEnter(Collision collision) {
		//Debug.Log (collision.gameObject.name);
		if (regex.Match(collision.gameObject.name).Index > -1) {
			Destroy (collision.gameObject, 3f);
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
