﻿using UnityEngine;
using System.Collections;

public class ScoreMenu : MonoBehaviour {
	int lastScore, highScore;
	TextMesh MenuText;
	string textContent;
	// Use this for initialization
	void Start () {
		lastScore = PlayerPrefs.GetInt ("lastScore", 0);
		highScore = PlayerPrefs.GetInt ("highScore", 0);
		MenuText = GetComponent<TextMesh> ();
		textContent = string.Format ("Last Score:\n{0}\nHigh Score:\n{1}", lastScore, highScore);
		MenuText.text = textContent;
	}
	
	// Update is called once per frame
	void Update () {
		/*if (Input.GetButtonDown ("Fire1") || GvrViewer.Instance.Triggered) {
			SceneManager.LoadScene (1);
		}*/
	}

	void LateUpdate(){
		GvrViewer.Instance.UpdateState ();
		if (GvrViewer.Instance.BackButtonPressed) {
			Application.Quit ();
		}
	}
}
