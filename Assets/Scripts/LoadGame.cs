﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadGame : MonoBehaviour {
	bool one_click, time_running;
	float time_of_click, delay;
	// Use this for initialization
	void Start () {
		one_click = false;
		time_running = false;
		delay = 0.4f;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			if (one_click && time_running) {
				SceneManager.LoadScene (1);
			} else {
				one_click = true;
				time_running = true;
				time_of_click = Time.time;
			}
		}
		if (time_running && Time.time - time_of_click > delay) {
			one_click = false;
			time_running = false;
		}
	}
}
